use std::{time, thread};
use console::Term;
mod objects;
use objects::{Rect, PhysEl};
use rand::Rng;

fn main() {
    let term = Term::stdout();
    let lines = init_console(&term);
    term.hide_cursor().unwrap();
    
    // arbitrary limit to number of objs
    let mut obj_list = Vec::with_capacity(100);
    
    for i in 0..20{
    let mut randgen = rand::thread_rng();
    let a = randgen.gen_range(-50..50);
    let b = randgen.gen_range(-50..50);

    let obj = Rect::new(vec![100 + i, 20],vec![1, 1],  vec![a, b], vec![0,-60], 1);
    obj_list.push(obj);
    }

    // ! inside frame loop
    let mut i = 0;
    
    while i < 1000 {
    
    let now = time::Instant::now();

    draw_frame(lines.clone(), &term, &mut obj_list);

    // limit framerate
    let elapsed = now.elapsed();
    let ms = time::Duration::from_millis(50);
    if !(elapsed > ms){
    thread::sleep(ms - elapsed);
    }
    i += 1;
    term.clear_to_end_of_screen().unwrap();

    }

}

fn init_console(term: &Term) -> Vec<Vec<String>>{
    // initialize things and get console size
    let term_size: (u16, u16)= term.size();
    let mut lines: Vec<Vec<String>> = Vec::with_capacity(term_size.1.into());
    
    for _ in 0..term_size.0{
        let mut vector1: Vec<String> = Vec::with_capacity(term_size.0.into());
        for _ in 0..term_size.1{
            vector1.push(" ".to_string());
        }
        lines.push(vector1);
    }
    return lines
}

fn draw_frame<T: PhysEl>(lines: Vec<Vec<String>>, term: &Term, obj_list: &mut Vec<T>)
where T: Clone {
        // TODO unecessary, remove this
        let mut buffer_lines = lines;
        let mut symb: String;
        // for object in object list: add to render frame
        // render frame is the frame to be rendered duh

        for obj in obj_list.iter_mut(){
            <T as PhysEl>::move_obj(obj);
            symb = obj.kind();
            let line_arr = obj.get_rect_arr();
            for (i, line) in line_arr.iter().enumerate(){
                for (k, _) in line.iter().enumerate(){
                    buffer_lines[obj.position()[1]+ k][obj.position()[0] + i] = symb.clone();
                }
            }
        }
    
        // reverse the lines because the engine is quirky uwu
        buffer_lines.reverse();
    
        // Draw the render frame
        for line in buffer_lines{
            let l = line.join("");
            term.write_line(l.as_str()).unwrap();
        }
    } 