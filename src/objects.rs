use std::convert::TryFrom;
use std::cmp::max;
use std::ops::{Neg};
use console::Term;
const ONE_FRAME: f64 = 0.05;


// the basic object struct, position has to be unsigned because of indexing at main

pub trait PhysEl{
    fn kind(&self) -> String;

    fn position(&self) -> Vec<usize>;

    fn set_velocity(&mut self, velocity: Vec<isize>);

    fn velocity(&self) -> Vec<isize>;

    fn force(&self) -> Vec<isize>;

    fn restitution(&self) -> f64;

    fn move_obj(&mut self);

    fn get_rect_arr(&self)-> Vec<Vec<&str>>;

    fn get_symb(id:u32)-> &'static str;

    fn get_rest(id: u32) -> f64;

    fn check_wall_collision(size: &Vec<usize>, pos: &mut Vec<usize>, vel: &mut Vec<isize>, restitution:&f64);

}

pub trait Weight{
    fn get_weight(&self) -> usize;

    fn set_weight(id:u32) -> usize;
}

impl Weight for Rect{
    fn get_weight(&self) -> usize {
        return self.weight;
    }
    
    fn set_weight(id: u32) -> usize{
        let weight: usize;
        let id = id;
        match id {
            1 => weight = 1,
            2 => weight = 10,
            _ => panic!("Invalid ID"),
        }
        return weight;
    }
}

impl PhysEl for Rect{
    fn position(&self) -> Vec<usize> {
        return vec![self.position[0], self.position[1]]
    }

    fn set_velocity(&mut self, velocity: Vec<isize>) {
        self.velocity = velocity;
    }

    fn get_rest(id: u32) -> f64 {
        let rest;
        match id {
            1 => rest = 0.8,
            2 => rest = 0.5,
            _ => panic!("Invalid ID!")
        }
        return rest
    }
    fn kind(&self) -> String{
        let symb: &str;
        match self.kind{
            1 => symb = "a",
            2 => symb = "@",
            _ => panic!("Invalid id!")
        };
        return symb.to_string();
    }

    fn restitution(&self) -> f64 {
        return self.restitution;
    }

    fn move_obj(&mut self){
        for i in 0..2{
            // calculate the velocity
            let curr_vel = self.velocity[i];
            let curr_accel = (self.force[i])/self.weight as isize;
            self.velocity[i] = (curr_vel as f64 + curr_accel as f64 * ONE_FRAME).round() as isize;

            // calculate the position, limit range to 0 so it doesnt complain
            let mut temp = isize::try_from(self.position[i]).unwrap();
            temp = (temp as f64 + self.velocity[i] as f64 * ONE_FRAME).round() as isize;
            temp = max(0, temp);
            self.position[i] = usize::try_from(temp).unwrap();
        }
        let rest = self.restitution();
        Rect::check_wall_collision(&self.size, &mut self.position, &mut self.velocity, &rest)
    }

    fn check_wall_collision(size: &Vec<usize>, pos: &mut Vec<usize>, vel: &mut Vec<isize>, restitution: &f64){
        // Check if the object is hitting the wall
        // flip the objects velocity if it is
        // kind of garbage solution, following the theme of this project
        let _term = Term::stdout();
        let term_size = Term::size(&_term);
        let size_vector = vec![term_size.1 -1, term_size.0 - 1];
        for i in 0..2{
            if (pos[i]+size[i])  >= size_vector[i].into(){
                pos[i] = (size_vector[i]- size[i] as u16).into();
                vel[i] = (vel[i].neg() as f64 * restitution) as isize;
            }
            else if (pos[i] <= 0) && vel[i].abs() > 5{
                pos[i] = 0;
                vel[i] = (vel[i].neg() as f64 * restitution) as isize ;
            }
        }

    }

    fn get_rect_arr(&self) -> Vec<Vec<&str>>{
        let mut arr:Vec<Vec<&str>> = Vec::with_capacity(self.size[1]);
        for _ in 0..self.size[1]{
            let mut line:Vec<&str> = Vec::with_capacity(self.size[0]);
            for _ in 0..self.size[0]{
                line.push(Rect::get_symb(self.kind));
            }
            arr.push(line)
        }
        return arr
    }

    fn get_symb(id: u32) -> &'static str{
        let symb: &str;
        match id{
            1 => symb = "a",
            2 => symb = "@",
            _ => panic!("Invalid id!")
        };
        return symb;
    }

    fn velocity(&self) -> Vec<isize> {
        return vec![self.velocity[0], self.velocity[1]]
    }

    fn force(&self) -> Vec<isize> {
        return vec![self.force[0], self.force[1]]
    }
    
}
#[derive(Debug, Clone)]
pub struct Rect {
    pub position: Vec<usize>,
    pub size: Vec<usize>,
    pub symbol: String,
    pub velocity: Vec<isize>,
    pub force: Vec<isize>,
    pub kind: u32,
    weight: usize,
    pub restitution: f64,
}
impl Rect {
    pub fn new<'b>(pos: Vec<usize>,size: Vec<usize>, vel: Vec<isize>, force: Vec<isize>, id: u32) -> Self{
        let symbol = Rect::get_symb(id);
        let weight = Rect::set_weight(id);
        let rest = Rect::get_rest(id);
        Rect{position: pos, size,  symbol: symbol.to_string(), velocity:vel, force,kind:id, weight, restitution: rest}
    }
}